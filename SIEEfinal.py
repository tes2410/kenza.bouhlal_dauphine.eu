import random
from matplotlib import pyplot as plt



#Revision / Rappels

print("hello, je révise Python :D ")

x= "Kenza"
print("Hello, " + x)

#programme pour se presenter
name = "Kenza"
age = "23"
city = "Gonesse"
job = "étudiante"
hobby = "cuisiner"
print("Je m'appelle", name, "; j'ai", age, "ans et j'habite à", city,". Je suis", job, "et dans la vie, j'aime", hobby,".")


#echanger 2 variables 
x = 7
y = 14
tampon = x
x = y 
y = tampon
print(x)
print(y)


#manipulation de nombres
a = 7
b = a + 3
print(b)


type("Hello")
type(17)
type("17")
type(1833830382)
type(x)
type(float("17"))
type(17*2)
type("17*2")


#Age à partir de la date de naissance
naiss=1999
an=2022
age=an-naiss
print("Tu as", age, "ans.")

#Disneyland
limite=140 #taille maximum pour faire l'attraction (en cm)
taille=165 #taille du visiteur (en cm)
if taille < limite:
    print("Vous ne pouvez pas faire cette attraction.")
else:
    print("C'est partiiiiii")
    
#permis de conduire
age =23 #normalement, demander l'âge de l'utilisateur mais éviter d'utiliser input dans le cadre de notre évaluation
if age>=18:
    print("peut conduire")
else:
    print("oops, vas prendre le bus")
    
#Bulletin scolaire
n = 13.48
if (n<10):
    print("Ajourné(e)")
elif (n<12):
    print("Passable")
elif(n<14):
    print("Assez Bien")
elif (n<16):
    print("Bien")
elif (n<18):
    print("Très Bien")
else:
    print("Félicitations")
    
#Le coffre fort
code = ""
nb_essai = 0
while code != "1003" and nb_essai != 10:
    nb_essai += 1
    code = random.randint(1000,9999)
    if code == "1003" :
        print("Bravo, l'argent est à vous !")
    else :
        print("Code invalide")
        
#Fonctions

def minimum(a,b):
    if(a>=b):
        res=b
    else:
        res=a
    return res

minimum(2,4)

def carre(x):
    y=x*x
    return(y)

carre(5)

def cube(x) :
    y = x**3
    return(y)

cube(5)

def sum_to(n):
    somme=0
    for i in range(1,n+1):
        somme=somme+i
    return somme

sum_to(6)


def palindrome(ch):
    debut = 0
    fin = len(ch)-1
    while (debut<fin) and (ch[debut]==ch[fin]):
        debut=debut+1
        fin=fin-1
    return debut >= fin

print("Entrer une chaîne de caractères") #normalement, on met un input mais ici problématique pour l'algorithme de l'évaluation

ch="ressasser"
if palindrome(ch):
    print(ch, "est un palindrome")
else:
    print(ch, "n'est pas un palindrome")
    
#Listes et dictionnaires

def premier(n): #fonction nb premier ou non
    for j in range(2,n): 
        if n%j==0: #Le reste doit être égal à 0
            return False
    return True

def liste_premiers(n): #liste nb premiers <= à n
    liste=[]
    for k in range(1,n+1):
        if premier(k)==True:
            liste.append(k)
    return liste

print(liste_premiers(20))
print(liste_premiers(1000))
print(len(liste_premiers(1000000)))

#prgrm qui va mettre en premier tous les chiffres pairs suivis des chiffres impairs de 0 à 10
L = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
new = []
for i in L:
    if i % 2 == 0:
        new.insert(0,i)
    else:
        new.append(i)
    print(new)

#Comparer 2 listes
l1 = [1, 2, 3, 4, 5]
l2 = [7, 3, 1, 8, 6]

if(set(l1) == set(l2)):
    print("Listes égales")
else:
    print("Listes non égales")
    
l1 = [1, 2, 3, 4, 5]
l2 = [9, 8, 7, 6, 5]
    
print("Element(s) communs aux 2 listes :", set(l1) & set(l2))

#avec une boucle for
l1 = [7,8,9]
l2 = [9,2,8]
for i in l1:
    for j in l2:
        if(i==j):
            print(i)
            break
      
#Jeu Pierre Papier Ciseaux contre l'ordinateur. Comme on ne peut pas demander à l'utilisateur de saisir à chaque fois son coup, on utilise la fonction aléatoire pour décider à la place du joueur. 
joueur="Kenza" #normalement on demande au joueur de saisir son nom avec input
nb_victoires = 0
pc_victoires = 0
nuls = 0

while True:
    print(joueur, ":", nb_victoires, "ordinateur :", pc_victoires, " égalité(s) :", nuls)
    coupJ = random.randint(1,3)
    if coupJ == 1:
        coupJ ="Pierre"
        print("Pierre")
    if coupJ == 2:
        coupJ ="Papier"
        print("Papier")
    else: 
        coupJ ="Ciseaux"
        print("Ciseaux")
        continue
    if coupJ == "Pierre":
        print("Pierre VS ...", end=" ")
    elif coupJ == "Papier":
        print("Papier VS ...", end=" ")
    else:
        print("Ciseaux VS ...", end=" ")
ordiRandom = random.randint(1,3)
if ordiRandom == 1:
    coupordi ="Pierre"
    print("Pierre")
if ordiRandom == 2:
    coupordi ="Papier"
    print("Papier")
else: 
    coupordi ="Ciseaux"
    print("Ciseaux")
if coupJ == coupordi:
    print("Egalité")
    nuls = nuls +1
elif coupJ =="Pierre" and coupordi =="Ciseaux":
    print("Le joueur gagne :D")
    nb_victoires = nb_victoires + 1
elif coupJ== "Papier" and coupordi =="Pierre":
    print("Le joueur gagne :D")
    nb_victoires = nb_victoires + 1
elif coupJ== "Ciseaux" and coupordi=="Papier":
    print("Le joeur gagne :D")
    nb_victoires = nb_victoires + 1
elif coupJ== "Pierre" and coupordi=="Papier":
    print("Le joueur perds :( ")
    pc_victoires = pc_victoires + 1
elif coupJ== "Papier" and coupordi=="Ciseaux":
    print("Le joueur perds :( ")
    pc_victoires = pc_victoires + 1
elif coupJ== "Ciseaux" and coupordi=="Pierre":
    print("Le joueur perds :( ")
    pc_victoires = pc_victoires + 1
    
    
#le pile ou face sans gains
print("Pile ou Face ? Choisis une option")
print ("Tu as bien choisi ? Ok, garde la réponse dans ta tête")
pileouface = random.randint(1,3)
if pileouface == 1:
    print("Pile.C'est ce que tu avais choisi ?")
elif pileouface == 2: 
    print("Face.C'est ce que tu avais choisi ?")
else: 
    print("Oops, sur le côté de la pièce. Tu ne l'avais pas vu venir hein...")
print("Il n'y a avait rien à gagner, tu peux disposer :D ")
    
    
#KohLanta
jeu = ['Boule Noire','Boule Blanche']

alea = random.randint(0,1)

print("Piochez dans le sac")
print("...")
print("Le résultat est ", jeu[alea])


#La phrase du jour
p_1 = "La vie est un long poème que l'on écrit soi-même."
p_2 = "Le bonheur en cette vie, c'est l'amour ! Il est si doux de s'aimer."
p_3 = " Quand l'amour vous fait signe, suivez-le."
p_4 = "S'aimer pour le meilleur en oubliant le pire."
p_5 = "Douce amitié vaut mieux qu'amour léger."
 
phrases = [p_1, p_2, p_3, p_4, p_5]
 
phrases_aleatoire = random.choice(phrases)
 
print(phrases_aleatoire)

#Cadavre exquis
femme = ["Une scientifique","Une reine maléfique","Une pirate des Caraïbes", "Une sorcière", "Une consultante", "Une influenceuse"]
homme = ["un policier", "Pablo Escobar", "ton grand-père", "un robot tueur", "un auditeur", "un youtubeur"]
lieu = ["à Zanzibar.", "à Lidl.", "à Dauphine.", "à Mazars.", "à la Cour des Comptes."]
ellePortait = ["des oreilles de chat.", "des ailes noires.", "un sac de congélation.", "une Fiat 500."]
ilPortait = ["un costume SpiderMan.", "un déguisement de fille.", "une serviette de plage."]
femmeDit = ["« Qui êtes-vous ? »", "« Qu'est-ce que tu me veux ? »", "« Pourquoi ? »"]
hommeDit = ["« AKHAAA ! »", "« C'est à moi ne touche pas! »", "« Je ne suis pas intéressé. »"]
consequence = ["la paix dans le monde", "le chaos", "la fin du monde", "des arcs-en-ciel", "un mariage", "une mort imminente"]
temoinDit = ["« C’est absurde ! »", "« Catastrophique. »", "« Ni fait ni à faire ! »"]
print (random.choice(femme), "a rencontré", random.choice(homme), random.choice(lieu))
print ("Elle portait", random.choice(ellePortait))
print ("Il portait", random.choice(ilPortait))
print ("Elle a dit", random.choice(femmeDit))
print ("Il a dit", random.choice(hommeDit))
print ("La consequence a été", random.choice(consequence))
print ("Le témoin de la scène a dit", random.choice(temoinDit))


#dictionnaires révision

a1 = { 'rac' : 4,
       'g' :{'rac' : 2,
             'g' : {},
             'd' : { 'rac' : 3, 'g' : {}, 'd' : {}}},
       'd' : {'rac' : 7,
              'g' : {'rac' : 5, 'g' : {}, 'd' : {'rac' : 6, 'g' : {}, 'd' : {}}},
              'd' : {'rac' : 10,
                     'g' : {'rac' : 9, 'g' : {}, 'd' : {}},
                     'd' : {'rac' : 14, 'g' : {}, 'd' : {}}}}}
print(a1)

def est_abr(a):
    if type(a)==dict:
        if a=={}:
            return True
        if list(a.keys())==['rac','g','d']:
            return est_abr(a['g']) and est_abr(a['d'])
        else:
            return False
    return False
            
print(est_abr(a1))
print(a1.keys())

# Faire un graph en Python. Axe X :
x = [3, 5, 9, 11]

# Les valeurs pour l'axe Y
y = [2, 4, 8, 16]

plt.plot(x, y)
plt.show()
